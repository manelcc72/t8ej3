package es.manelcc.t8ej3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView cuenta;
    private Button contar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cuenta = (TextView) findViewById(R.id.cuenta);
        contar = (Button) findViewById(R.id.contar);
        contar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = Integer.parseInt(cuenta.getText().toString());
                n++;
                cuenta.setText("" + n);
            }
        });


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("CUENTA", cuenta.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cuenta.setText(savedInstanceState.getString("CUENTA"));
    }
}
